<?php

use App\Http\Controllers\CastController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\BioDataController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::get('/', [IndexController::class, 'dasboard']);
route::get('/daftar', [BioDataController::class, 'Bio']);
route::post('/home', [BioDataController::class, 'kirim']);
route::get('/table', [IndexController::class, 'table']);
route::get('/data-table', [IndexController::class, 'data_table']);

// tambah data
route::get('/cast/create',[CastController::class, 'create']);
// route form
route::post('/cast',[CastController::class, 'store']);
// tampil data
route::get('/cast',[CastController::class, 'index']);
// detail
route::get('/cast/{id}',[CastController::class, 'show']);
route::get('/cast/{id}/edit',[CastController::class, 'edit']);
// update ke database berdasarkan id
route::put('cast/{id}', [CastController::class, 'update']);
route::delete('cast/{id}', [CastController::class, 'destroy']);