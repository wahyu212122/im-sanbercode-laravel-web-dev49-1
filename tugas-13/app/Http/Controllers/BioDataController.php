<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BioDataController extends Controller
{
    public function Bio(){
        return view('tugas12.form');
    }

    public function kirim(Request $request){
        $namaDepan = $request->input('namaDepan');
        $namaBelakang = $request->input('namaBelakang');
      
        return view('tugas12.welcome', ['namadepan'=>$namaDepan, 'namabelakang'=>$namaBelakang]);

    }
}
