<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function dasboard(){
        return view('home');
    }
    public function table(){
        return view('table');
    }

    public function data_table(){
        return view('data-table');
    }
}